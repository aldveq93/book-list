function ctrlScript() {
    const doc = document,
          bookList = doc.querySelector('.table-body');
          newBook = new UI();
    // Listeners 
    // Add a new book to the list
    doc.querySelector('.form-book').addEventListener('submit', (e) =>{
        e.preventDefault();
        newBook.addNewBook();
    });
    // Delete a specific book from the list
    doc.getElementById('delete-book').addEventListener('click', (e) =>{
        if (bookList.childElementCount > 0) {
            newBook.deleteBook();
        } else {
            newBook.messageAlert('The list is empty!','error');
        }
    });
}

// Book class Declaration
class Book {
    constructor(bookName, bookAuthor, bookIsbn){
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookIsbn = bookIsbn;
    }

    returnBook(){
        let book = [this.bookName, this.bookAuthor, this.bookIsbn];
        return book; 
    }
}

// UI interactions
class UI {
    addNewBook() {
        const doc = document;
        let bookName = doc.getElementById('bookName').value,
            bookAuthor = doc.getElementById('bookAuthor').value,
            bookIsbn = doc.getElementById('bookIsbn').value;

        if (bookName == '' || bookAuthor == '' || bookIsbn == '') {
            this.messageAlert('Please, fill in all the inputs!','error');        
        } else {
            const book = new Book(bookName, bookAuthor, bookIsbn);
            this.buildRowBook(book);
        }
    }

    buildRowBook(book) {
        const doc = document,
          tableBody = doc.getElementById('table-body'),
          row = doc.createElement('tr');
          row.className = 'row-book';

        let i = 0,
            bookArray = book.returnBook();
            
        for(i; i < bookArray.length; i++){
            row.innerHTML += `<td class='table-book__column-name'>${bookArray[i]}</td>`;
        }

        tableBody.appendChild(row);
        this.clearFormInputs();
        this.selectedRow();
        this.messageAlert('Book added!','success');
    }

    clearFormInputs() {
        const doc = document;
        doc.querySelector('.form-book').reset();
    }

    selectedRow() {
        const doc = document,
          bookRows = [...doc.querySelectorAll('.row-book')];
    
        bookRows.forEach((row) =>{
            row.addEventListener('click',(e)=>{
                e.stopImmediatePropagation();
                this.tableRowSelected(e.target);
            }); 
        });
    }

    deleteBook(){           
        const rowsSelected = this.validateSelectedRowFromList();

        if(rowsSelected.length == 0){
            this.messageAlert('Please, select a book from the list!','error');
        } else {
            let i = 0;

            for (i; i < rowsSelected.length; i++){
                rowsSelected[i].remove();
                this.messageAlert('Book removed!','success');
                break;
            }
        }
    }

    validateSelectedRowFromList() {
        const doc = document,
              bodyTable = doc.getElementById('table-body'),
              bodyTableChildren = [...bodyTable.children];
        let boolRowsSelected;

        boolRowsSelected = bodyTableChildren.filter((row) =>{
            return row.classList.contains('row-selected');
        });
        return boolRowsSelected;
    }

    tableRowSelected(rowTarget) {
        if(rowTarget.parentElement.classList.contains('row-book')){
            if (rowTarget.parentElement.classList.contains('row-selected')){
                this.clearRowsSelected();
                rowTarget.parentElement.classList.remove('row-selected')
            } else {
                this.clearRowsSelected();
                rowTarget.parentElement.classList.add('row-selected')
            }
        }
    }

    clearRowsSelected(){
        const doc = document,
              bookRows = doc.querySelectorAll('.row-book');
        bookRows.forEach((row) =>{
            row.classList.remove('row-selected');
        });
        
    }

    messageAlert(message, typeMessage) {
        const doc = document,
              messageContainer = doc.querySelector('.message');
          
        messageContainer.textContent = `${message}`;
        messageContainer.classList.add(`${typeMessage}`);
        messageContainer.classList.add('active');

        setTimeout(()=>{
            doc.querySelector('.message').classList.remove('active');
            doc.querySelector('.message').classList.remove(`${typeMessage}`);
            doc.querySelector('.message').textContent = '';
        },3000);
    }
}

ctrlScript();
